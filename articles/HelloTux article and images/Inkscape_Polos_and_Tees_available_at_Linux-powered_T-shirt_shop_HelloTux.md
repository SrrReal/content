# Inkscape Polos and Tees available at Linux-powered T-shirt shop HELLOTUX

Being an Inkscaper never looked so good! The wait for a sharp Inkscape-branded T-shirt is over. If you're a Linux user or developer, then this is the perfect fit for you.

Inkscape merchandise is now available on HELLOTUX <https://www.hellotux.com/>, an online T-shirt shop with a passion for Linux and free software.

HELLOTUX embroiders T-shirts, sweatshirts and polos for Linux and free-software projects and donates a portion from each sale back to the project.


## About HELLOTUX

Gábor Kum’s passion for serving this market grew from his use of Linux that began in 1999. He is a Linux system administrator and software developer. This Hungarian entrepreneur made the first Linux T-shirts in 2002, which led to the family’s Hungarian Linux T-shirt shop, <https://www.pingvinbolt.hu/> in 2009. The international shop, HELLOTUX <https://www.hellotux.com/>, followed a year later.

As Kum explains on his site, “We are not just another T-shirt shop with some Linux shirts – we are a shop with only Linux and free software shirts. It does really matter.” The site boasts products that are “100% made with Linux”.



HELLOTUX will donate USD $3 from each Inkscape shirt sale to the project. So, show your Inkscape pride! Make a difference by giving back to the project and help it to remain free for all users.


Watch for more opportunities to buy Inkscape merchandise in the near future.

Head to HELLOTUX for your Inkscape embroidered tee or polo shirt (Shop link: <https://www.hellotux.com/inkscape>) now!